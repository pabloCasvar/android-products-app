package pablocasvar.com.productos_api.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import pablocasvar.com.productos_api.R;
import pablocasvar.com.productos_api.model.Product;

/**
 * Created by Pablo on 04/02/2019.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder>{

    private ArrayList<Product> productsList;

    public ProductAdapter(ArrayList<Product> productsList) {
        this.productsList = productsList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.row_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.productNameTextView.setText(productsList.get(position).getNombre());
        holder.productDescriptionTextView.setText(productsList.get(position).getDescripcion());
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder{

        TextView productNameTextView;
        TextView productDescriptionTextView;

        public ProductViewHolder(View itemView) {
            super(itemView);
            this.productNameTextView = itemView.findViewById(R.id.tv_product_name);
            this.productDescriptionTextView = itemView.findViewById(R.id.tv_product_description);
        }
    }
}
