package pablocasvar.com.productos_api.service;

import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pablo on 03/02/2019.
 */

public class RetrofitInstance {

    private static Retrofit retrofit = null;
    private static String BASE_URL = "https://api-products-casvar.herokuapp.com/";

    public static ProductsService getService(){

        if(retrofit == null){
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit.create(ProductsService.class);
    }
}
