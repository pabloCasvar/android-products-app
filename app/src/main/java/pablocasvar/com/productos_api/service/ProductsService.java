package pablocasvar.com.productos_api.service;

import java.util.ArrayList;

import pablocasvar.com.productos_api.model.LoginRequest;
import pablocasvar.com.productos_api.model.LoginResponse;
import pablocasvar.com.productos_api.model.Product;
import pablocasvar.com.productos_api.model.ProductRequest;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Pablo on 03/02/2019.
 */

public interface ProductsService {

    @GET("product")
    Call<ArrayList<Product>> getProducts(@Header("Authorization") String token);

    @POST("product")
    Call<Product> saveProduct(
            @Header("Authorization") String token,
            @Body ProductRequest product
    );

    @POST("user/authenticate")
    Call<LoginResponse> login(
            @Body LoginRequest authentication
    );
}
