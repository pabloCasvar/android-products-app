package pablocasvar.com.productos_api.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pablocasvar.com.productos_api.R;
import pablocasvar.com.productos_api.adapter.ProductAdapter;
import pablocasvar.com.productos_api.model.Product;
import pablocasvar.com.productos_api.service.ProductsService;
import pablocasvar.com.productos_api.service.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListProductFragment extends Fragment {

    private ArrayList<Product> products;
    private ProductAdapter productAdapter;
    private RecyclerView recyclerView;
    public MainActivity myActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getProducts();

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_product, container, false);
    }

    public ArrayList<Product> getProducts() {

        ProductsService productsService = RetrofitInstance.getService();
        myActivity = (MainActivity) getActivity();
        String token = myActivity.token;
        Call<ArrayList<Product>> call = productsService.getProducts("Bearer " + token);

        call.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
                products = response.body();

                for(Product product:products){
                    Log.i("Item", ""+product.getNombre());
                }

                viewData();
            }

            @Override
            public void onFailure(Call<ArrayList<Product>> call, Throwable t) {

            }
        });
        return products;
    }

    private void viewData(){
        recyclerView = (RecyclerView) getView().findViewById(R.id.rv_products_list);
        productAdapter = new ProductAdapter(products);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productAdapter);
    }

}
