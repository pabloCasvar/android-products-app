package pablocasvar.com.productos_api.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pablocasvar.com.productos_api.R;
import pablocasvar.com.productos_api.model.Product;
import pablocasvar.com.productos_api.model.ProductRequest;
import pablocasvar.com.productos_api.service.ProductsService;
import pablocasvar.com.productos_api.service.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProductFragment extends Fragment {

    public MainActivity myActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_product, container, false);
        Button button_add_product = (Button) view.findViewById(R.id.bt_add_product);
        final EditText edit_product_name = (EditText) view.findViewById(R.id.et_product_name);
        final EditText edit_product_image = (EditText) view.findViewById(R.id.et_product_image);
        final EditText edit_product_description = (EditText) view.findViewById(R.id.et_product_description);

        button_add_product.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                Log.i("Button for adding", "");

                String name;
                String imageUrl;
                String description;

                name = edit_product_name.getText().toString();
                imageUrl = edit_product_image.getText().toString();
                description = edit_product_description.getText().toString();

                if(name.trim().equals("") || imageUrl.trim().equals("") || description.trim().equals("")){
                    Toast.makeText(getContext() , "Ingresa todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    saveProduct(name, imageUrl, description);
                }
            }
        });
        return view;
    }

    public void saveProduct(String name, String imageUrl, String description) {
        myActivity = (MainActivity) getActivity();
        String token = myActivity.token;
        ProductsService productsService = RetrofitInstance.getService();
        ProductRequest product = new ProductRequest(
                                    name,
                                    imageUrl,
                                    description);

        Call<Product> call = productsService.saveProduct(
                "Bearer " + token,
                product
                );

        call.enqueue(new Callback<Product>() {
             @Override
             public void onResponse(Call<Product> call, Response<Product> response) {
                 if(response.code() == 200){
                     Toast.makeText(getContext(), "Producto guardado", Toast.LENGTH_SHORT).show();

                     FragmentManager fm = getActivity().getSupportFragmentManager();
                     fm.beginTransaction().replace(R.id.container, new ListProductFragment()).commit();
                 } else {
                     Toast.makeText(getContext(), "Error, intente más tarde", Toast.LENGTH_SHORT).show();
                 }
             }

             @Override
             public void onFailure(Call<Product> call, Throwable t) {
                 Toast.makeText(getContext(), "Error inesperado, intente de nuevo más tarde", Toast.LENGTH_SHORT).show();
             }
        });

    }
}
