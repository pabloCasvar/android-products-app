package pablocasvar.com.productos_api.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pablocasvar.com.productos_api.R;
import pablocasvar.com.productos_api.model.LoginRequest;
import pablocasvar.com.productos_api.model.LoginResponse;
import pablocasvar.com.productos_api.service.ProductsService;
import pablocasvar.com.productos_api.service.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    public MainActivity myActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        Button button_login = (Button) view.findViewById(R.id.bt_login);
        final EditText edit_username = (EditText) view.findViewById(R.id.et_username);
        final EditText edit_password = (EditText) view.findViewById(R.id.et_password);

        button_login.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                String username;
                String password;
                username = edit_username.getText().toString();
                password = edit_password.getText().toString();

                if(username.trim().equals("") || password.trim().equals("")){
                    Toast.makeText(getContext() , "Ingresa todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    userLogin(username, password);
                }
            }
        });
        return view;
    }

    private void userLogin(String username, String password) {
        ProductsService productsService = RetrofitInstance.getService();
        LoginRequest user = new LoginRequest(
                username,
                password);

        Call<LoginResponse> call = productsService.login(
                user
        );

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.code() == 200){
                    String token = response.body().getToken();
                    myActivity = (MainActivity) getActivity();
                    myActivity.token = token;
                    Toast.makeText(getContext(), "Token recibido:" + token, Toast.LENGTH_SHORT).show();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.beginTransaction().replace(R.id.container, new ListProductFragment()).commit();
                } else {
                    Toast.makeText(getContext(), "Usuario o contraseña incorrecto", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Error inesperado, intente de nuevo más tarde", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
